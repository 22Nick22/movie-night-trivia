## Usage

1.  Create a database locally
2.  Clone this repository
3.  `cp .env.example .env`
4.  Set database environment variables
5.  `composer install`
6.  `php artisan key:generate`
7.  `php artisan migrate --seed`
8.  `php artisan serve`
