<?php

use Illuminate\Support\Facades\Route;

Auth::routes();

Route::get('/', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/leaderboard', App\Http\Controllers\LeaderboardController::class)->name('leaderboard');

Route::get('/game', [App\Http\Controllers\GameController::class, 'create']);
Route::post('/game', [App\Http\Controllers\GameController::class, 'store']);
