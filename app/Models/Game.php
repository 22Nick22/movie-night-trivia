<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Game extends Model
{
    use HasFactory;

    protected $fillable = [
        'correct',
        'total',
    ];

    protected static function boot()
    {
        parent::boot();

        static::saving(function ($model) {
            $model->score_percentage = $model->correct / $model->total * 100;
        });
    }
}
