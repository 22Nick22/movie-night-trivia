<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreGameRequest;
use DOMDocument;
use DOMXPath;

class GameController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Scrapes the movie data and returns the questions and answers for the specified number of rounds.
     */
    public function create(): \Illuminate\Http\JsonResponse
    {
        libxml_use_internal_errors(true);
        $dom = new DOMDocument();
        $dom->loadHtmlFile(config('mnt.IMDB_URL'));
        $xpath = new DOMXPath($dom);
        $nodes = $xpath->query('//td[@class="titleColumn"]');

        $questions = collect();

        foreach ($nodes as $node) {
            $title = $node->getElementsByTagName('a')->item(0)->nodeValue;
            $year = (int) trim($node->getElementsByTagName('span')->item(0)->nodeValue, '()');
            $answers = [['answer' => $year, 'correct' => true]];

            //Generate random years around the correct year
            $range = range($year - 10, $year + 10);
            unset($range[10]);

            foreach (collect($range)->random(3) as $year) {
                $answers[] = ['answer' => $year, 'correct' => false];
            }

            shuffle($answers); //Ensure true answer is not always in the same position

            $questions->push([
                'question' => "When was {$title} released?",
                'answers' => $answers,
            ]);
        }

        return response()->json($questions->random(config('mnt.rounds')));
    }

    /**
     * Store the results of a completed game, returning the score as a string.
     */
    public function store(StoreGameRequest $request): \Illuminate\Http\JsonResponse
    {
        $game = auth()->user()->games()->create($request->validated());
        $score = $game->correct * config('mnt.correct_points');
        $score = $score + ($game->total - $game->correct) * config('mnt.incorrect_points');
        $scoreMax = $game->total * config('mnt.correct_points');

        return response()->json("{$score}/{$scoreMax} ({$game->score_percentage}%)", 201);
    }
}
