<?php

namespace App\Http\Controllers;

use App\Models\User;

class LeaderboardController extends Controller
{
    public function __invoke()
    {
        $users = User::withCount('games')
            ->withAvg('games', 'score_percentage')
            ->limit(10)
            ->orderBy('games_avg_score_percentage', 'DESC')
            ->get();

        return view('leaderboard', compact('users'));
    }
}
