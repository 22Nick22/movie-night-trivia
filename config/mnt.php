<?php

return [
    /*
    |--------------------------------------------------------------------------
    | IMDB URL
    |--------------------------------------------------------------------------
    |
    | This is the URL where the movie title and year of release should be scraped from.
    |
    */

    'IMDB_URL' => env('IMDB_URL', 'http://www.imdb.com/chart/top?ref_=nb_mv_3_chttp'),

    /*
    |--------------------------------------------------------------------------
    | Rounds
    |--------------------------------------------------------------------------
    |
    | This value determines the how many questions should be per game.
    |
    */

    'rounds' => env('ROUNDS', 8),

    /*
    |--------------------------------------------------------------------------
    | Correct Points
    |--------------------------------------------------------------------------
    |
    | This value determines how many points should be awarded for a correct answer.
    |
    */

    'correct_points' => env('CORRECT_POINTS', 5),

    /*
    |--------------------------------------------------------------------------
    | Incorrect Points
    |--------------------------------------------------------------------------
    |
    | This value determines how many points should be awarded for an incorrect answer.
    |
    */

    'incorrect_points' => env('INCORRECT_POINTS', -3),
];
